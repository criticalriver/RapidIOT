========
Modules
========



   .. container:: contents

      .. container:: textblock

         Here is a list of all modules:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  \ `SCL communication            | APIs for communicating with      |
         | AP                               | Network Processor                |
         | I <group__communication.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  \ `SCL Wi-Fi                    | APIs for controlling the Wi-Fi   |
         | API <group__wifi.html>`__        | system                           |
         +----------------------------------+----------------------------------+

.. toctree::
   :maxdepth: 8
   :hidden:
   
   group__communication.rst
   group__wifi.rst
   

