================
Data Structures
================


   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +-----------------------------------+-----------------------------------+
         |  C\ `network_param                | Network parameters structure      |
         | s <structnetwork__params.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         |  C\ `SCL_EMA                      |                                   |
         | C <class_s_c_l___e_m_a_c.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_inter                    |                                   |
         | face_shared_info_t <structscl__in |                                   |
         | terface__shared__info__t.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         |                                   | Structure for storing 802.11      |
         | C\ `scl_listen_interval_t <struct | powersave listen interval values  |
         | scl__listen__interval__t.html>`__ | See scl_wifi_get_listen_interval  |
         |                                   | for more information              |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_                         | Structure for storing a MAC       |
         | mac_t <structscl__mac__t.html>`__ | address (Wi-Fi Media Access       |
         |                                   | Control address)                  |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_sca                      | Structure for storing extended    |
         | n_extended_params_t <structscl__s | scan parameters                   |
         | can__extended__params__t.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_scan_result              | Structure for storing scan        |
         | <structscl__scan__result.html>`__ | results                           |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_simple_scan_result       | Structure to store scan result    |
         |  <structscl__simple__scan__       | parameters for each AP            |
         |  result.html>`__                  |                                   |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_ss                       | Structure for storing a Service   |
         | id_t <structscl__ssid__t.html>`__ | Set Identifier (i.e               |
         +-----------------------------------+-----------------------------------+
         |  C\ `scl_tx                       | SCL transmit buffer structure     |
         | _buf <structscl__tx__buf.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         |  C\ `SclAccessPoint               | `SclAccessPoint                   |
         |  <class_scl_access_point.html>`__ | <class_scl_access_point.html>`__  |
         |                                   | class                             |
         +-----------------------------------+-----------------------------------+
         |  C\ `SclSTAInterface <c           | `SclSTAInterface <c               |
         | lass_scl_s_t_a_interface.html>`__ | lass_scl_s_t_a_interface.html>`__ |
         |                                   | class Implementation of the       |
         |                                   | Network Stack for the SCL         |
         +-----------------------------------+-----------------------------------+
         |  C\ `wl_bss_info_struct <str      | BSS(Basic Service Set)            |
         | uctwl__bss__info__struct.html>`__ | information structure             |
         +-----------------------------------+-----------------------------------+

.. toctree::
   :maxdepth: 8
   :hidden:
   
   structnetwork__params.rst
   class_s_c_l___e_m_a_c.rst
   structscl__interface__shared__info__t.rst
   structscl__listen__interval__t.rst
   structscl__mac__t.rst
   structscl__scan__extended__params__t.rst
   structscl__scan__result.rst
   structscl__simple__scan__result.rst
   structscl__ssid__t.rst
   structscl__tx__buf.rst
   class_scl_access_point.rst
   class_scl_s_t_a_interface.rst
   structwl__bss__info__struct.rst
   
   