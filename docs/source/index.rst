.. RapidIOT documentation master file, created by
   sphinx-quickstart on Wed Jul  8 16:39:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RapidIOT's documentation!
====================================

.. toctree::
   :maxdepth: 8
   :hidden:
   
   SCL/index.rst
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
